const { Engine } = require('@fieldlink/workflow-engine');
const knex = require('knex');
const knexFile = require('./knexfile');

// process.env.NODE_ENV = 'test';
// const memorySettings = require("@fieldlink/workflow-engine/settings/settings");
// const engine = new Engine(...memorySettings.persist_options);

const engine = new Engine(
    'knex',
    knex(knexFile[process.env.NODE_ENV])
    // knex({
    //     client: 'pg',
    //     connection: {
    //         host: "127.0.0.1",
    //         port: 5439,
    //         user: "postgres",
    //         password: "postgres",
    //         database: "workflow"
    //     }
    // })
    // knex({
    //     client: 'pg',
    //     connection: {
    //         host: "workflow_postgres",
    //         user: "postgres",
    //         password: "postgres",
    //         database: "workflow"
    //     }
    // })
);

module.exports = engine;