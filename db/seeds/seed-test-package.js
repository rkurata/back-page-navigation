const uuid = require("uuid/v1");
const { core_package,
  test_package } = require("./packages/packages");
const pageBlueprint = require('../../workflow/blueprint/pageBlueprint');

exports.seed = function (knex, Promise) {
  // Deletes ALL existing entries
  return knex('workflow').del().then(
    function () {
      return knex('workflow').insert([
        {
          id: uuid(),
          created_at: new Date(),
          name: 'pageExample',
          description: 'pageExample',
          blueprint_spec: JSON.stringify(pageBlueprint)
        }
      ])
    }
  ).then(
    function () {
      return knex('packages').del()
        .then(function () {
          // Inserts seed entries
          return knex('packages').insert([
            {
              id: uuid(),
              created_at: new Date(),
              name: "test_package",
              description: "Workflow Lisp test package",
              code: JSON.stringify(test_package)
            },
            {
              id: uuid(),
              created_at: new Date(),
              name: "core",
              description: "Workflow Lisp core package",
              code: JSON.stringify(core_package)
            }
          ]);
        });
    }
  )
};
