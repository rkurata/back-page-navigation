const mqtt = require('async-mqtt');

let client;
async function connect() {
    try {
        client = await mqtt.connectAsync('ws://broker.hivemq.com:8000/mqtt');
    } catch (error) {
        console.error('Error at mqtt connect');
        console.error(error);
    }
    return;
}

async function sendMessage(topic, message) {
    if (client && client.connected) {
        await client.publish(topic, JSON.stringify(message));
    }
}

module.exports = {
    sendMessage: sendMessage,
    connect: connect,
}