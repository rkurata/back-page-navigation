const jsonWebToken = require('jsonwebtoken');

function getToken(context, next) {
    const actorId = context.params.id;
    const { claims } = context.request.body;
    const token = jsonWebToken.sign({
        actorData: {
            id: actorId,
            claims: claims,
        }
    }, '1234');

    context.body = {
        token: token
    }
}

module.exports = {
    getToken: getToken,
};
