const engine = require('../engine');
const cockpit = require('../cockpit');

async function createProcess(context, next) {
    const workflowName = context.params.name;
    const { actorData } = context.state.user;
    const process = await engine.createProcessByWorkflowName(workflowName, actorData, { creatorId: actorData.id });
    if (process) {
        context.body = process.serialize();
    }
};

async function runProcess(context, next) {
    const processId = context.params.id;
    const { actorData } = context.state.user;
    const { input } = context.request.body;
    const process = await engine.runProcess(processId, actorData, input);
    if (process) {
        context.status = 202;
    }
};

async function getProcess(context, next) {
    const processId = context.params.id;
    const process = await engine.fetchProcess(processId);
    context.body = process;
}

async function getActivityManager(context, next) {
    const activityManagerId = context.params.id;
    const { actorData } = context.state.user;
    const activityManager = await engine.fetchActivityManager(activityManagerId, actorData);
    if (activityManager) {
        console.log(activityManager);
        context.body = activityManager;
    } else {
        context.status = 403;
    }
}

async function getActivityManagerForActor(context, next) {
    const { actorData } = context.state.user;
    const activityManagers = await engine.fetchAvailableActivitiesForActor(actorData);
    context.body = activityManagers;
}

async function getActivityManagerForProcess(context, next) {
    const processId = context.params.id;
    const { actorData } = context.state.user;
    const activityManager = await engine.fetchAvailableActivityForProcess(processId, actorData);
    if ( activityManager ) {
        context.body = activityManager;
    } else {
        context.status = 403;
    }
}

async function commitActivity(context, next) {
    const processId = context.params.id;
    const { actorData } = context.state.user;
    const { input } = context.request.body;
    const activityManager = await engine.commitActivity(processId, actorData, input);
    if (activityManager) {
        if (activityManager.error) {
            context.status = 500;
        } else {
            context.status = 202;
        }
    }
}

async function pushActivity(context, next) {
    const processId = context.params.id;
    const { actorData } = context.state.user;
    const activityManager = await engine.pushActivity(processId, actorData);
    if (activityManager) {
        context.body = activityManager;
    }
}

async function submitActivity(context, next) {
    const processId = context.params.id;
    const { actorData} = context.state.user;
    const { input } = context.request.body;
    let activityManager = await engine.commitActivity(processId, actorData, input);
    if (!activityManager || activityManager.error) {
        console.error(activityManager);
        context.status = 500;
    } else {
        activityManager = await engine.pushActivity(processId, actorData);
        if (activityManager) {
            console.error(activityManager);
            context.body = activityManager;
        } else {
            context.status = 500;
        }
    }
}

async function getWorkflow(context, next) {
    const { actorData } = context.state.user;
    let workflows = await cockpit.getWorkflowsForActor(actorData);
    context.body = workflows
}

module.exports = {
    createProcess: createProcess,
    runProcess: runProcess,
    getProcess: getProcess,
    getActivityManager: getActivityManager,
    getActivityManagerForActor: getActivityManagerForActor,
    getActivityManagerForProcess: getActivityManagerForProcess,
    commitActivity: commitActivity,
    pushActivity: pushActivity,
    submitActivity: submitActivity,
    getWorkflow: getWorkflow,
}