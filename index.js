const engine = require('./engine');
const baseBlueprint = require('./workflow/blueprint/baseBlueprint');
const forcaBlueprint = require('./workflow/blueprint/forcaBlueprint');
const pageBlueprint = require('./workflow/blueprint/pageBlueprint');
const analysisDefault = require('./workflow/blueprint/analysisDefault');
const analysisSuccess = require('./workflow/blueprint/analysisSuccess');
const analysisError = require('./workflow/blueprint/analysisError');
const analysisExample = require('./workflow/blueprint/analysisExample');
const analysisProgressbar = require('./workflow/blueprint/analysisProgressbar');
const server = require('./server');
const mqttClient = require('./mqttClient');
const notifier = require('./notifier');

async function saveBlueprint({name, description, blueprint}) {
    const workflow = await engine.saveWorkflow(name, description, blueprint);
    console.log(`Created workflow "${name}" with id "${workflow.id}"`);
}

async function start() {
    // await saveBlueprint({name: 'base', description: 'base', blueprint: baseBlueprint});
    // await saveBlueprint({name: 'forca', description: 'forca', blueprint: forcaBlueprint});
    // await saveBlueprint({name: 'pageExample', description: 'page', blueprint: pageBlueprint});
    // await saveBlueprint({name: 'analysisDefault', description: 'analysis default', blueprint: analysisDefault});
    // await saveBlueprint({name: 'analysisSuccess', description: 'analysis success', blueprint: analysisSuccess});
    // await saveBlueprint({name: 'analysisError', description: 'analysis error', blueprint: analysisError});
    // await saveBlueprint({name: 'analysisExample', description: 'analysis analysisExample', blueprint: analysisExample});
    // await saveBlueprint({name: 'analysisProgressbar', description: 'analysis analysisProgressbar', blueprint: analysisProgressbar});
    await mqttClient.connect();
    notifier.activateNotifier();
    console.log('Server up');
}

start()