const Router = require('@koa/router')
const workflowController = require('../controller/workflowController');

const router = new Router({
    prefix: '/workflow'
});

router.post('/:name/process', workflowController.createProcess);
router.options('/:name/process', (context) => {
    context.set('Access-Control-Allow-Method', 'POST');
    context.set('Access-Control-Allow-Headers', ['Content-Type', 'Authorization']);
    context.status = 204;
});
router.post('/process/:id', workflowController.runProcess);
router.options('/process/:id', (context) => {
    context.set('Access-Control-Allow-Method', 'POST');
    context.set('Access-Control-Allow-Headers', ['Content-Type', 'Authorization']);
    context.status = 204;
});
router.get('/process/:id', workflowController.getProcess);
router.get('/process/activityManager/list', workflowController.getActivityManagerForActor);
router.options('/process/activityManager/list', (context) => {
    context.set('Access-Control-Allow-Method', 'GET');
    context.set('Access-Control-Allow-Headers', ['Authorization']);
    context.status = 204;
});
router.get('/process/activityManager/:id', workflowController.getActivityManager);
router.options('/process/activityManager/:id', (context) => {
    context.set('Access-Control-Allow-Method', 'GET');
    context.set('Access-Control-Allow-Headers', ['Authorization']);
    context.status = 204;
});
router.get('/process/:id/activityManager/', workflowController.getActivityManagerForProcess);
router.options('/process/:id/activityManager/', (context) => {
    context.set('Access-Control-Allow-Method', 'GET');
    context.set('Access-Control-Allow-Headers', ['Authorization']);
    context.status = 204;
});
router.post('/process/:id/activityManager/activity/commit', workflowController.commitActivity);
router.options('/process/:id/activityManager/activity/commit', (context) => {
    context.set('Access-Control-Allow-Method', 'POST');
    context.set('Access-Control-Allow-Headers', ['Content-Type', 'Authorization']);
    context.status = 204;
});
router.post('/process/:id/activityManager/activity/push', workflowController.pushActivity);
router.options('/process/:id/activityManager/activity/push', (context) => {
    context.set('Access-Control-Allow-Method', 'POST');
    context.set('Access-Control-Allow-Headers', ['Content-Type', 'Authorization']);
    context.status = 204;
});
router.post('/process/:id/activityManager/activity/submit', workflowController.submitActivity);
router.options('/process/:id/activityManager/activity/submit', (context) => {
    context.set('Access-Control-Allow-Method', 'POST');
    context.set('Access-Control-Allow-Headers', ['Content-Type', 'Authorization']);
    context.status = 204;
});
router.get('/', workflowController.getWorkflow);
router.options('/', (context) => {
    context.set('Access-Control-Allow-Method', 'GET');
    context.set('Access-Control-Allow-Headers', ['Authorization']);
    context.status = 204;
});

module.exports = router;