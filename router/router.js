const Router = require('@koa/router');
const workflowRouter = require('./workflowRouter');
const permissionRouter = require('./permissionRouter');

const router = new Router();
router.use(workflowRouter.routes());
router.use(permissionRouter.routes());

module.exports = router;