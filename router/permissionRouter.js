const Router = require('@koa/router');

const permissionController = require('../controller/permissionController');

const router = new Router({
    prefix: '/permission'
});


router.post('/:id/token', permissionController.getToken);
router.options('/:id/token', (context) => {
    context.set('Access-Control-Allow-Method', 'POST');
    context.set('Access-Control-Allow-Headers', ['Content-Type']);
    context.status = 204;
});

module.exports = router;