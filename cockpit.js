const { Cockpit } = require('@fieldlink/workflow-engine');
const knex = require('knex');

// process.env.NODE_ENV = 'test';
// const memorySettings = require("@fieldlink/workflow-engine/settings/settings");
// const engine = new Engine(...memorySettings.persist_options);

const cockpit = new Cockpit(
    'knex',
    knex({
        client: 'pg',
        connection: {
            host: "127.0.0.1",
            port: 5439,
            user: "postgres",
            password: "postgres",
            database: "workflow"
        }
    })
);

module.exports = cockpit;