const { sendMessage } = require('./mqttClient');
const engine = require('./engine');

async function processStateListener(processState) {
    console.log(processState);
    const topic = `/notify/process/${processState._process_id}`;
    const message = {
        processId: processState._process_id,
    };

    await sendMessage(topic, message);
}

async function activityManagerListener(activityManager) {
    const topic = `/notify/process/${activityManager._process_id}/activity_manager/${activityManager._id}`
    const message = {
        processId: activityManager._process_id,
        activityManagerId: activityManager._id,
    };

    await sendMessage(topic, message);
}

function activateNotifier() {
    engine.setProcessStateNotifier(processStateListener);
    engine.setActivityManagerNotifier(activityManagerListener);
}

module.exports = {
    activateNotifier: activateNotifier,
};
