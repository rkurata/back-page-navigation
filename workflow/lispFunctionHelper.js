module.exports = {
    get return_true() {
        return ["fn", ["&", "args"], true];
    },

    validate_claim(valid_claim) {
        return ["fn", ["actor_data", "bag"],
            ["eval",
                ["apply", "or",
                    [
                        "map",
                        [
                            "fn",
                            ["v"],
                            ["=", "v", ["`", valid_claim]],
                        ],
                        ["get", "actor_data", ["`", "claims"]],
                    ]
                ]
            ]
        ]
    },
}