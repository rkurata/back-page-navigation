const lispFunctionHelper = require('../lispFunctionHelper');

module.exports = {
    requirements: ["core"],
    prepare: [],
    nodes: [
        {
            id: "1",
            type: "Start",
            name: "Start node",
            next: "2",
            lane_id: "1"
        },
        {
            id: "2",
            type: "Finish",
            name: "Finish node",
            next: null,
            lane_id: "1"
        }
    ],
    lanes: [
        {
            id: "1",
            name: "the_only_lane",
            rule: lispFunctionHelper.return_true
        }
    ]
};