const lispFunctionHelper = require('../lispFunctionHelper');

module.exports = {
    requirements: ["core"],
    prepare: [],
    nodes: [
      {
        id: "1",
        type: "Start",
        name: "Start node",
        next: "2",
        lane_id: "1"
      },
      {
        id: "2",
        type: "IdentityUserNativeTask",
        name: "User task node",
        next: "99",
        lane_id: "1",
        parameters: {
          action: "analysis",
          input: {
            progressbar: {
              status: "success",
              value: 99,
            },
            errors: []
          }
        }
      },
      {
        id: "99",
        type: "Finish",
        name: "Finish node",
        next: null,
        lane_id: "1"
      }
    ],
    lanes: [
      {
        id: "1",
        name: "default",
        rule: lispFunctionHelper.return_true
      }
    ]
  };