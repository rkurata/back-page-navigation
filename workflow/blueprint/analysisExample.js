const lispFunctionHelper = require('../lispFunctionHelper');

module.exports = {
  requirements: ["core"],
  prepare: [],
  nodes: [
    {
      id: "1",
      type: "Start",
      name: "Start node",
      next: "2",
      lane_id: "1"
    },
    {
      id: "2",
      type: "IdentityUserNativeTask",
      name: "User task node",
      next: "99",
      lane_id: "1",
      parameters: {
        "action": "ANALYSIS_ERROR",
        "input": {
          "data": [
            {
              "type": "selfie",
              "button_action": "CONTINUE_PROCESS",
              "errors": [
                {
                  "title": "Arquivo em Formato Inválido",
                  "description": "Precisamos que você nos envie sua foto segurando o documento de identificação em algum dos seguintes formatos: JPEG, PNG, GIF, PDF.\n Antes de enviar o arquivo verifique se está nítido."
                }
              ]
            },
            {
              "type": "Extrato do Benefício",
              "button_action": "CONTINUE_PROCESS",
              "errors": [
                {
                  "title": "Arquivo Ilegível",
                  "description": "Enviar seu extrato de benefício legível.\nNão será aceito fotocópia.\nAntes de enviar o arquivo, verifique se está nítido."
                }
              ]
            }
          ]
        }
      }
    },
    {
      id: "99",
      type: "Finish",
      name: "Finish node",
      next: null,
      lane_id: "1"
    }
  ],
  lanes: [
    {
      id: "1",
      name: "default",
      rule: lispFunctionHelper.return_true
    }
  ]
};