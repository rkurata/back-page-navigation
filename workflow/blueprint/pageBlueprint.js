const lispFunctionHelper = require('../lispFunctionHelper');
module.exports = {
    requirements: ['core'],
    prepare: [],
    nodes: [{
            id: '1',
            type: 'Start',
            name: 'Start node',
            next: '2',
            lane_id: '1',
        },
        {
            id: '2',
            type: 'IdentityUserNativeTask',
            name: 'Login',
            next: '3',
            lane_id: '1',
            parameters: {
                action: 'loginPage',
                input: {}
            }
        },
        {
            id: '3',
            type: 'SetToBagTask',
            name: 'Set login to bag',
            next: '4',
            lane_id: '1',
            parameters: {
                input: {
                    login: {
                        $ref: 'result.activities[0].data.login'
                    }
                }
            }
        },
        {
            id: '4',
            type: 'IdentityUserNativeTask',
            name: 'Entry Value',
            next: '5',
            lane_id: '1',
            parameters: {
                action: 'valuePage',
                input: {}
            }
        },
        {
            id: '5',
            type: 'ScriptTask',
            name: 'Check user value',
            next: '6',
            lane_id: '1',
            parameters: {
                input: {
                    userValue: {
                        $ref: 'result.activities[0].data.userValue'
                    }
                },
                script: {
                    package: '',
                    function: [
                        'fn',
                        ['input', '&', 'args'],
                        [
                            'set',
                            [
                                'set',
                                {},
                                ['`', 'userValue'],
                                ['get', 'input', ['`', 'userValue']]
                            ],
                            ['`', 'automatic'],
                            [
                                'if',
                                [
                                    '<',
                                    ['get', 'input', ['`', 'userValue']],
                                    100
                                ],
                                ['`', 'approved'],
                                ['`', 'request'],
                            ]
                        ]
                    ]
                }
            }
        },
        {
            id: '6',
            type: 'SetToBagTask',
            name: 'Set userValue to bag',
            next: '7',
            lane_id: '1',
            parameters: {
                input: {
                    userValue: {
                        $ref: 'result.userValue'
                    },
                    automatic: {
                        $ref: 'result.automatic'
                    }
                }
            }
        },
        {
            id: '7',
            type: 'Flow',
            name: 'Limit decision',
            next: {
                approved: '20',
                request: '666',
                default: '666',
            },
            lane_id: '1',
            parameters: {
                input: {
                    automatic: {
                        $ref: 'result.automatic'
                    }
                }
            }
        },
        {
            id: '20',
            type: 'ScriptTask',
            name: 'Script task example',
            next: '21',
            lane_id: '1',
            parameters: {
                input: {},
                script: {
                    package: '',
                    function: [
                        'fn',
                        ['&', 'args'],
                        100
                    ]
                }
            }
        },
        {
            id: '21',
            type: 'IdentityUserNativeTask',
            name: 'Approved',
            next: '99',
            lane_id: '1',
            parameters: {
                action: 'approved',
                input: {
                    userValue: {
                        $ref: 'bag.userValue'
                    }
                }
            }
        },
        {
            id: '30',
            type: 'IdentityUserNativeTask',
            name: 'Disapproved',
            next: '99',
            lane_id: '1',
            parameters: {
                action: 'disapproved',
                input: {
                    userValue: {
                        $ref: 'bag.userValue'
                    }
                }
            }
        },
        {
            id: '10',
            type: 'IdentityUserNativeTask',
            name: 'Check user requested value',
            next: '11',
            lane_id: '2',
            parameters: {
                action: 'manualApproval',
                input: {
                    userValue: {
                        $ref: 'bag.userValue'
                    },
                    login: {
                        $ref: 'bag.login'
                    }
                }
            }
        },
        {
            id: '11',
            type: 'Flow',
            name: 'Check if user approved',
            next: {
                approved: '20',
                disapproved: '30',
                default: '30',
            },
            lane_id: '2',
            parameters: {
                input: {
                    evaluation: {
                        $ref: 'result.activities[0].data.evaluation'
                    }
                }
            }
        }, 
        {
            id: "666",
            type: "SleepSystemTask",
            name: "Sleep system task node",
            next: "10",
            lane_id: "1",
            parameters: {
                timeout: 10,
            },
        },
        {
            id: '99',
            type: 'Finish',
            name: 'Finish node',
            next: null,
            lane_id: '1',
        }
    ],
    lanes: [{
            id: '1',
            name: 'clientClaim',
            rule: [
                "fn",
                ["actor_data", "bag"],
                [
                    "or",
                    [
                        "eval",
                        [
                            "apply",
                            "or",
                            [
                                "map",
                                [
                                    "fn", ["v"],
                                    ["=", "v", ["`", 'client']]
                                ],
                                ["get", "actor_data", ["`", "claims"]]
                            ]
                        ]
                    ],
                    [
                        "eval",
                        [
                            "apply",
                            "or",
                            [
                                "map",
                                [
                                    "fn", ["v"],
                                    ["=", "v", ["`", 'admin']]
                                ],
                                ["get", "actor_data", ["`", "claims"]]
                            ]
                        ]
                    ]
                ]
            ],
        },
        {
            id: '2',
            name: 'mangerClaim',
            rule: [
                "fn",
                ["actor_data", "bag"],
                [
                    "or",
                    [
                        "eval",
                        [
                            "apply",
                            "or",
                            [
                                "map",
                                [
                                    "fn", ["v"],
                                    ["=", "v", ["`", 'manager']]
                                ],
                                ["get", "actor_data", ["`", "claims"]]
                            ]
                        ]
                    ],
                    [
                        "eval",
                        [
                            "apply",
                            "or",
                            [
                                "map",
                                [
                                    "fn", ["v"],
                                    ["=", "v", ["`", 'admin']]
                                ],
                                ["get", "actor_data", ["`", "claims"]]
                            ]
                        ]
                    ]
                ]
            ]
        }
    ]
}