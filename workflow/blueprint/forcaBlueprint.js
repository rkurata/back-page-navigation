const lispFunctionHelper = require('../lispFunctionHelper');

module.exports = {
  requirements: ["core"],
  prepare: [],
  nodes: [
    {
      id: "1",
      type: "Start",
      name: "Start node",
      next: "2",
      lane_id: "1"
    },
    {
      id: "2",
      type: "ScriptTask",
      name: "Generate initial values",
      next: "3",
      lane_id: "1",
      parameters: {
        input: [],
        script: {
          package: "",
          function: [
            "fn",
            ["&", "args"],
            [
              "let",
              [
                "index",
                [
                  "js",
                  [
                    ".",
                    "Math",
                    ["`", "floor"],
                    [
                      "*",
                      [".", "Math", ["`", "random"]],
                      [".", "Math", ["`", "floor"], 3],
                    ],
                  ],
                ],
                "words",
                [
                  "list",
                  ["`", "test"],
                  ["`", "game"],
                  ["`", "example"],
                ],
                "word",
                ["nth", "words", "index"],
                "state",
                [
                  "js",
                  [
                    "str",
                    ["`", "let word = '"],
                    "word",
                    ["`", "';word.replace(/./g, '_');"],
                  ],
                ],
              ],
              [
                "set",
                [
                  "set",
                  {
                    errorCount: 0,
                    isPrivate: true,
                  },
                  ["`", "state"],
                  "state"
                ],
                ["`", "word"],
                "word"
              ]
            ],
          ],
        },
      }
    },
    {
      id: "3",
      type: "SetToBagTask",
      name: "Set initial values to the bag",
      next: "4",
      lane_id: "1",
      parameters: {
        input: [
          {
            namespace: "result",
            keys: [
              "word",
              "state",
              "errorCount",
              "isPrivate",
            ],
          },
        ],
        output: [
          {
            key: "word",
            destination: "word",
          },
          {
            key: "state",
            destination: "state",
          },
          {
            key: "errorCount",
            destination: "errorCount",
          },
          {
            key: "isPrivate",
            destination: "isPrivate"
          },
        ],
      },
    },
    {
      id: "4",
      type: "IdentityUserNativeTask",
      name: "User input letter",
      next: "5",
      lane_id: "1",
      parameters: {
        action: "letterInput",
        input: [
          {
            namespace: "bag",
            keys: [
              "state",
              "errorCount",
              "isPrivate",
            ],
          },
        ],
        output: [
          {
            key: "activities.[0]",
            destination: "userInput",
          },
        ],
      },
    },
    {
      id: "5",
      type: "ScriptTask",
      name: "test",
      next: "6",
      lane_id: "1",
      parameters: {
        input: [
          {
            namespace: "result",
            keys: ["userInput"],
          },
          {
            namespace: "bag",
            keys: ["creatorId"]
          }
        ],
        script: {
          package: "",
          function: [
            "fn",
            ["input", "&", "args"],
            [
              "let",
              [
                "inputData",
                [
                  "get",
                  ["get", "input", ["`", "userInput"]],
                  ["`", "data"],
                ],
                "letter",
                [
                  "get",
                  "inputData",
                  ["`", "letter"]
                ],
                "changePermission",
                [
                  "if",
                  [
                    "=",
                    ["get", "input", ["`", "creatorId"]],
                    ["get", "inputData", ["`", "actorId"]]
                  ],
                  true,
                  false
                ]
              ],
              [
                "set",
                [
                  "set",
                  {},
                  ["`", "inputType"],
                  [
                    "if",
                    ["null?", "letter"],
                    [
                      "if",
                      "changePermission",
                      ["`", "changePermission"],
                      ["`", "unauthorized"],
                    ],
                    ["`", "letterInput"],
                  ],
                ],
                ["`", "letterInput"],
                "letter"
              ]
            ]
          ]
        }
      },
    },
    {
      id: "6",
      type: "SetToBagTask",
      name: "Set input letter on bag",
      next: "7",
      lane_id: "1",
      parameters: {
        input: [
          {
            namespace: "result",
            keys: ["letterInput", "inputType"]
          }
        ],
        output: [
          {
            key: "letterInput",
            destination: "letterInput"
          }
        ]
      }
    },
    {
      id: "7",
      type: "Flow",
      name: "Control change permission or letter input",
      next: {
        unauthorized: "4",
        changePermission: "8",
        letterInput: "10",
        default: "4",
      },
      lane_id: "1",
      parameters: {
        input: [
          {
            namespace: "result",
            keys: ["inputType"]
          }
        ]
      }
    },
    {
      id: "8",
      type: "ScriptTask",
      name: "Change permission",
      next: "9",
      lane_id: "1",
      parameters: {
        input: [
          {
            namespace: "bag",
            keys: ["isPrivate"]
          }
        ],
        script: {
          package: "",
          function: [
            "fn",
            ["input", "&", "args"],
            [
              "set",
              {},
              ["`", "isPrivate"],
              ["not", ["get", "input", ["`", "isPrivate"]]]
            ],
          ],
        },
      },
    },
    {
      id: "9",
      type: "SetToBagTask",
      name: "Change isPrivate",
      next: "4",
      lane_id: "1",
      parameters: {
        input: [
          {
            namespace: "result",
            keys: ["isPrivate"]
          }
        ],
        output: [
          {
            key: "isPrivate",
            destination: "isPrivate"
          }
        ]
      }
    },
    {
      id: "10",
      type: "ScriptTask",
      name: "Check input letter",
      next: "11",
      lane_id: "1",
      parameters: {
        input: [
          {
            namespace: "bag",
            keys: [
              "letterInput",
              "word",
              "state",
              "errorCount"
            ]
          }
        ],
        script: {
          package: "",
          function: [
            "fn",
            ["input", "&", "args"],
            [
              "js",
              [
                "str",
                ["`", "let input = "],
                "input",
                [
                  "`",
                  "; let letters = input.state.split(''); let index = input.word.indexOf(input.letterInput); \
                    let error = index === -1; while(index !== -1) { \
                      letters[index] = input.letterInput; index = input.word.indexOf(input.letterInput, index + 1); \
                    }; \
                    let result = {errorCount: error ? input.errorCount + 1 : input.errorCount, state: letters.join('')};result;\
                  ",
                ],
              ],
            ],
          ],
        },
      },
    },
    {
      id: "11",
      type: "SetToBagTask",
      name: "Update error count and state",
      next: "12",
      lane_id: "1",
      parameters: {
        input: [
          {
            namespace: "result",
            keys: [
              "errorCount",
              "state"
            ]
          }
        ],
        output: [
          {
            key: "errorCount",
            destination: "errorCount",
          },
          {
            key: "state",
            destination: "state",
          },
        ],
      },
    },
    {
      id: "12",
      type: "ScriptTask",
      name: "Check error count",
      next: "13",
      lane_id: "1",
      parameters: {
        input: [
          {
            namespace: "bag",
            keys: [
              "state",
              "word",
              "errorCount"
            ],
          },
        ],
        script: {
          package: "",
          function: [
            "fn",
            ["input", "&", "args"],
            [
              "set",
              {},
              ["`", "nextStep"],
              [
                "if",
                [
                  "=",
                  ["get", "input", ["`", "state"]],
                  ["get", "input", ["`", "word"]],
                ],
                ["`", "victory"],
                [
                  "if",
                  [
                    "=",
                    ["get", "input", ["`", "errorCount"]],
                    6,
                  ],
                  ["`", "defeat"],
                  ["`", "continue"],
                ],
              ],
            ],
          ],
        },
      },
    },
    {
      id: "13",
      type: "Flow",
      name: "Check next step",
      next: {
        victory: "14",
        defeat: "15",
        continue: "4",
        default: "4",
      },
      lane_id: "1",
      parameters: {
        input: [
          {
            namespace: "result",
            keys: ["nextStep"],
          },
        ],
      },
    },
    {
      id: "14",
      type: "IdentityUserNativeTask",
      name: "User victory",
      next: "99",
      lane_id: "1",
      parameters: {
        action: "victory",
        input: [
          {
            namespace: "bag",
            keys: ["state"],
          },
        ],
        output: [],
      },
    },
    {
      id: "15",
      type: "IdentityUserNativeTask",
      name: "User defeat",
      next: "99",
      lane_id: "1",
      parameters: {
        action: "defeat",
        input: [
          {
            namespace: "bag",
            keys: ["errorCount"],
          }
        ],
        output: [],
      },
    },
    {
      id: "99",
      type: "Finish",
      name: "Finish node",
      next: null,
      lane_id: "1",
    },
  ],
  lanes: [
    {
      id: "1",
      name: "default",
      rule: [
        "fn",
        ["actor_data", "bag"],
        [
          "if",
          [
            "get",
            "bag",
            ["`", "isPrivate"]
          ],
          [
            "=",
            [
              "get",
              "bag",
              ["`", "creatorId"]
            ],
            [
              "get",
              "actor_data",
              ["`", "id"]
            ]
          ],
          lispFunctionHelper.return_true
        ]
      ]
    }
  ]
};