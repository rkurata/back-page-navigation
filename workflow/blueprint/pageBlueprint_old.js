const lispFunctionHelper = require('../lispFunctionHelper');

module.exports = {
    requirements: ['core'],
    prepare: [],
    nodes: [
        {
            id: '1',
            type: 'Start',
            name: 'Start node',
            next: '2',
            lane_id: '1',
        },
        {
            id: '2',
            type: 'IdentityUserNativeTask',
            name: 'Login',
            next: '3',
            lane_id: '1',
            parameters: {
                action: 'loginPage',
                input: [],
                output: [
                    {
                        key: 'activities.[0].data.login',
                        destination: 'login',
                    },
                ],
            },
        },
        {
            id: '3',
            type: 'SetToBagTask',
            name: 'Set login to bag',
            next: '4',
            lane_id: '1',
            parameters: {
                input: [
                    {
                        namespace: 'result',
                        keys: ['login'],
                    },
                ],
                output: [
                    {
                        key: 'login',
                        destination: 'login',
                    },
                ],
            },
        },
        {
            id: '4',
            type: 'IdentityUserNativeTask',
            name: 'Entry Value',
            next: '5',
            lane_id: '1',
            parameters: {
                action: 'valuePage',
                input: [],
                output: [
                    {
                        key: 'activities.[0].data.userValue',
                        destination: 'userValue'
                    }
                ],
            },
        },
        {
            id: '5',
            type: 'ScriptTask',
            name: 'Check user value',
            next: '6',
            lane_id: '1',
            parameters: {
                input: [
                    {
                        namespace: 'result',
                        keys: ['userValue'],
                    }
                ],
                script: {
                    package: '',
                    function: [
                        'fn',
                        ['input', '&', 'args'],
                        [
                            'set',
                            [
                                'set',
                                {},
                                ['`', 'userValue'],
                                ['get', 'input', ['`', 'userValue']]
                            ],
                            ['`', 'automatic'],
                            [
                                'if',
                                [
                                    '<',
                                    ['get', 'input', ['`', 'userValue']],
                                    100
                                ],
                                ['`', 'approved'],
                                ['`', 'request'],
                            ],
                        ],
                    ],
                },
            },
        },
        {
            id: '6',
            type: 'SetToBagTask',
            name: 'Set userValue to bag',
            next: '7',
            lane_id: '1',
            parameters: {
                input: [
                    {
                        namespace: 'result',
                        keys: ['userValue', 'automatic'],
                    },
                ],
                output: [
                    {
                        key: 'userValue',
                        destination: 'userValue',
                    },
                ],
            },
        },
        {
            id: '7',
            type: 'Flow',
            name: 'Limit decision',
            next: {
                approved: '20',
                request: '10',
                default: '10',
            },
            lane_id: '1',
            parameters: {
                input: [
                    {
                        namespace: 'result',
                        keys: ['automatic']
                    }
                ]
            }
        },
        {
            id: '20',
            type: 'ScriptTask',
            name: 'Script task example',
            next: '21',
            lane_id: '1',
            parameters: {
                input: [],
                script: {
                    package: '',
                    function: [
                        'fn',
                        ['&', 'args'],
                        100
                    ]
                }
            }
        },
        {
            id: '21',
            type: 'IdentityUserNativeTask',
            name: 'Approved',
            next: '99',
            lane_id: '1',
            parameters: {
                action: 'approved',
                input: [
                    {
                        namespace: 'bag',
                        keys: ['userValue']
                    },
                ],
                output: [],
            },
        },
        {
            id: '30',
            type: 'IdentityUserNativeTask',
            name: 'Disapproved',
            next: '99',
            lane_id: '1',
            parameters: {
                action: 'disapproved',
                input: [
                    {
                        namespace: 'bag',
                        keys: ['userValue']
                    },
                ],
                output: [],
            },
        },
        {
            id: '10',
            type: 'IdentityUserNativeTask',
            name: 'Check user requested value',
            next: '11',
            lane_id: '2',
            parameters: {
                action: 'manualApproval',
                input: [
                    {
                        namespace: 'bag',
                        keys: ['userValue', 'login']
                    }
                ],
                output: [
                    {
                        key: 'activities.[0].data.evaluation',
                        destination: 'evaluation'
                    },
                ],
            },
        },
        {
            id: '11',
            type: 'Flow',
            name: 'Check if user approved',
            next: {
                approved: '20',
                disapproved: '30',
                default: '30',
            },
            lane_id: '2',
            parameters: {
                input: [
                    {
                        namespace: 'result',
                        keys: ['evaluation']
                    },
                ],
            },
        },
        {
            id: '99',
            type: 'Finish',
            name: 'Finish node',
            next: null,
            lane_id: '1',
        },
    ],
    lanes: [
        {
            id: '1',
            name: 'clientClaim',
            rule: [
                "fn",
                ["actor_data", "bag"],
                [
                    "or",
                    [
                        "eval",
                        [
                            "apply",
                            "or",
                            [
                                "map",
                                [
                                    "fn", ["v"], ["=", "v", ["`", 'client']]
                                ],
                                ["get", "actor_data", ["`", "claims"]]
                            ]
                        ]
                    ],
                    [
                        "eval",
                        [
                            "apply",
                            "or",
                            [
                                "map",
                                [
                                    "fn", ["v"], ["=", "v", ["`", 'admin']]
                                ],
                                ["get", "actor_data", ["`", "claims"]]
                            ]
                        ]
                    ]
                ]
            ],
        },
        {
            id: '2',
            name: 'mangerClaim',
            rule: [
                "fn",
                ["actor_data", "bag"],
                [
                    "or",
                    [
                        "eval",
                        [
                            "apply",
                            "or",
                            [
                                "map",
                                [
                                    "fn", ["v"], ["=", "v", ["`", 'manager']]
                                ],
                                ["get", "actor_data", ["`", "claims"]]
                            ]
                        ]
                    ],
                    [
                        "eval",
                        [
                            "apply",
                            "or",
                            [
                                "map",
                                [
                                    "fn", ["v"], ["=", "v", ["`", 'admin']]
                                ],
                                ["get", "actor_data", ["`", "claims"]]
                            ]
                        ]
                    ]
                ]
            ]
        }
    ]
}