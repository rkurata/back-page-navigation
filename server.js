const Koa = require('koa');
const bodyParser = require('koa-bodyparser');
const jwt = require('koa-jwt');
const router = require('./router/router');

const server = new Koa();

server.use(async (context, next) => {
    await next();
    context.set('Access-Control-Allow-Origin', '*');
})
server.use(jwt({ secret: '1234', passthrough: true}));
server.use(bodyParser());
server.use(router.routes());
// server.use(async context => {
//     context.body = 'Invalid route'
// });

server.listen(8081);

module.exports = server;